<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Paul',
            'email' => 'paul@test.com',
            'password' => '$2y$10$v2dmUcs86A5dWW7RHOrqreKjKjdir7OQuVrGA.i6G7hLw58iDdiaW',
        ]);
    }
}
