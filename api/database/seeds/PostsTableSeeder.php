<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            'title' => 'First Post',
            'content' => 'This is my first Post',
            'slug' => 'first-post',
            'user_id' => 1
        ]);
    }
}
