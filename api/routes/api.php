<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', 'LoginController@login');
Route::post('logout', 'LoginController@logout');
Route::post('register', 'UserController@store');

Route::get('posts/{slug}', 'PostController@show');
Route::patch('posts/{slug}', 'PostController@update');
Route::delete('posts/{slug}', 'PostController@delete');
Route::resource('posts', 'PostController');


Route::get('posts/{slug}/comments', 'CommentController@index');
Route::post('posts/{slug}/comments', 'CommentController@store');
Route::patch('posts/{slug}/comments/{comment}', 'CommentController@update');
Route::resource('comments', 'CommentController');
