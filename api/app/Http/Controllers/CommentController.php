<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\StoreCommentsRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $post = Post::where('slug', $slug)->first();

        return response()->json(['data' => Comment::where('commentable_id', $post->id)->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($slug, StoreCommentsRequest $request)
    {
        $data = $request->all();
        $post = Post::where('slug', $slug)->first();
        $data['commentable_id'] = $post->id;
        $data['creator_id'] = Auth::id();
        $data['commentable_type'] = "AppPosts";

        return response()->json(['data' => Comment::create($data)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Comment $comment, StoreCommentsRequest $request)
    {
        $data = $request->all();

        $post = Post::where('slug', $slug)->first();

        $comment = Comment::where('id', $comment->id)->where('commentable_id', $post->id)->update($data);

        return response()->json(['data' => Comment::find($comment)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
